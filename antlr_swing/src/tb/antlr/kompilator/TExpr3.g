tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
// prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";
prog    : (e+=range | e+=expr | d+=decl)* -> program(nazwa={$e},dek={$d});

range : ^(BEGIN {enterScope();} (range | e+=expr | d+=decl)* {leaveScope();}) -> block(w={$e},dek={$d})
      ;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st}) 
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> podstaw(p1={$e1.text},p2={$e2.st})
        | INT -> int(i={$INT.text})
        | ID -> id(name={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;} -> if_else(p1={$e1.st},p2={$e2.st},p3={$e3.st},numer={numer.toString()})
    ;
    