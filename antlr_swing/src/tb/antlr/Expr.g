grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | block)+ EOF!;

block
    : LCB^ (stat | block)* RCB!
    ;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | NL ->
//   | LCB NL -> LCB   // left curly brace  
//   | RCB NL -> RCB   // right curly brace
    | if_statement NL -> if_statement
    ;

if_statement
    : IF^ expr THEN! (block|expr) (ELSE! (block|expr))?
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : pwExpr
      ( MUL^ pwExpr
      | DIV^ pwExpr
      | MOD^ pwExpr
      )*
    ;

pwExpr
  : atom
  (POW^ atom
  )*
  ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

IF : 'if';

ELSE : 'else';

THEN : 'then';

VAR :'var';

PRINT: 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

LCB
  : '{'
  ;

RCB
  : '}'
  ;

MOD
  : '%'
  ;

POW
  : '^'
  ;

 