tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
import tb.antlr.symbolTable.LocalSymbols;
}
@members {
LocalSymbols local_symbols = new LocalSymbols();
}

// prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ; do wykomentowania, trzeba rozszerzyc

prog :( 
//  ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
    e=expr {drukuj ($e.text + " = " + $e.out.toString());}
  | ^(VAR i1=ID) {local_symbols.newSymbol($i1.text);}
  | ^(PODST i1=ID e2=expr) {local_symbols.setSymbol($i1.text, $e2.out);}
//  | LCB {local_symbols.enterScope();}
//  | RCB {local_symbols.leaveScope();}
  )*
  ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {if($e2.out==0){drukuj("dzielenie przez zero, error"); $out=-1;}
                                    else $out = $e1.out / $e2.out;}
//        | ^(PODST i1=ID   e2=expr) do wykomentowania, dodac reszte dzialan
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(POW   e1=expr e2=expr) {$out = (int)Math.pow($e1.out, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID {$out = local_symbols.getSymbol($ID.text);}
        ;

       